#!/bin/bash
#Remplace les retours à la ligne qui commence par une tabulation \fp (et supprime le retour)
find . -name "*.txt" -exec sed -i ':a;N;$!ba;s/\n\t/ \\fp /g' {} \;

