#!/bin/bash
#Ce script permet de fusionner des notes numérotées avec le chapitre et le verset séparé par une virgule. Fichier notes **_notes.txt, fichier bible *.usfm
for item in {0..99}

do printf -v id "%02d" "$item"; echo "${id}.usfm, ${id}_notes.txt"
awk  -i inplace -v INPLACE_SUFFIX=.old  '$1~/^[0-9]+\,[0-9]+\./ { tmp=$1; $1=""; refs[tmp]=$0; $1=tmp}; $1=="\\c" {rf=$2}; $1=="\\v" && /\*/ { sub(/ \*/,"\\f + \\fr "rf","$2" \\ft "refs[rf","$2"."]"\\f\*",$0)}; {print}'  "${id}_notes.txt"  "${id}.usfm"
done
