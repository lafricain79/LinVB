1,1.	Liloba lya Bomoi : Liloba lya Nzambe lizali liziba lya bomoi (tala Mbk 4,1 ; 32,47 ; Mt 4,4). Nkombo eye epesami awa na Mwana wa Nzambe, oyo afandaki o ntei ya bapostolo.
1,4.	Esengo ya bino, to : esengo ya biso.
1,5.	Nzambe azali Mwinda : Bosantu bonso bouti epai ya ye. Yezu amipesi mpe nkombo eye, esika alobi : « Ngai nazali mwinda mwa mokili », lokola ’te oyo ayeli mokili bosolo.
1,7.	Biso banso tozali lisanga se lyoko, mpo Nzambe azali Mwinda (bosembo mpe bolingi). Bato bazali na ye lisa­nga, bakozala na mwinda, na bosembo mpe na bolingi soko bakokisi mibeko mya ye.
1,10.	O Minkanda Misantu ekomami polele ’te moto nyonso azali mosumuki : Nz 51,7 ; 143,2 ; Yob 4,17 ; Biy 20,9 ; Rom 3,9 ; Yak 3,2.
2,8.	Mobeko mwa sika : mozali mwa sika zambi moyeli bato mateya ma sika, mateya ma bomoi mpe ma liwa lya Kristu.
2,10.	Akotuta... : To : akopesa ndakisa ebe te.
2,13.	Oyo azali ut’o libandela : Kristu.
2,15.	Bolinga mokili te : Ezali mokili moye te Nzambe akeli kitoko, moye mokosepelisaka biso bato mpe. Mokili mozali mpe baninga ba biso te, ata moke te. Mokili moye Yoane atangi awa mozali nde : mateya ma lokuta, mateya manso ma nguya iye izali kobuna na Nzambe mpe na Kristu, na mpe bato banso baboyi koyamba ’te Nzambe amimonisi na boyei bwa Kristu.
2,18.	–Tala 2 Tes 2,1-3.
	–Monguna wa Kristu : Bato banso baboyi koyamba ’te Nzambe Mwana akomi moto.
2,20.	Bosili bokuli Elimo : Bapakoli bino Mafuta masantu.
2,22.	Oyo akolobaka ’te... : ’te Yezu moto wa Nazarete azali Mwana wa Nzambe mpe Mobikisi.
3,8.	Azali moto wa zabolo : zambi azali kolanda mayele mabe na mpe ezalela eye zabolo azali kolakisa ye. Bakobengaka moto ona ‘Mwana wa ye mpenza’, mpamba te, o makambo manso azali koulana na ye, lokola mwana aulani na tata wa ye.
3,9.	Tala Yo 1,12-13 ; 3,6 ; 1 Pe 1,23.
3,19-20.	Mokristu ayebi solo ’te azali komikosa te awa amiyebi mwana wa Nzambe, soko akokisi mobeko mwa bolingi o misala mya ye minso.
3,22.	Akopesa biso yango : Mokristu oyo azali kotosa mibeko mya Nzambe minso, mpe koluka kosala se maye mazali kosepelisa Nzambe, akoki kosenge Nzambe eloko yoko te eyokani na ndi­nga ya Nzambe te ; Nzambe mpe akopima ye eloko te.
4,1-6.	Bobele moto oyo ayambi ’te Kristu azali Mesiya mpe Mobikisi, akoki kozala profeta wa solo.
4,12.	Ata moto moko te akoki komono Nzambe na miso, nzokande mokristu ayebi solo ’te azali na bomoi bwa Nzambe, zambi azali na bolingi.
5,1.	Yoane alakisi ’te moto oyo alingi Nzambe, asengeli kolinga bana ba Nzambe lokola.
5,4.	Mokili : Tala 2,15+.
5,6.	–Na makila, awa batuboli mopanzi mwa Yezu.
	–Bato basusu bazalaki koteya ’te Kristu abikisi bato bobele na batisimo ya ye ; yango wana S. Yoane akomi polele ’te Kristu abikisi bango na liwa lya ye o kuruse lokola.
5,16.	Nani asali lisumu likokamba ye o liwa ? Moto abwaki boyambi bwa ye.
