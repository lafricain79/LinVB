1,1.	Moto alingi bosembo, oyo azali semba o miso ma Nzambe, akomeka koyeba malamu maye Nzambe alingi ; bongo engebene na maye makomami o Mobeko mwa Nzambe mpe na maye motema mwa ye mokoyebisa ye akobongisa ezalela ya ye.
1,4.	Motema mwa moto mozali mabe te, kasi akosengela kobunda mpo ’te mokoma moombo wa masumu te.
1,5.	Na bwanya mpe na nguya ya ye, Nzambe akokotelaka bato kin’o kati ya mitema mpo ya kokamba mpe kolendisa bango o makambo manso. Mokomi wa buku ya Bwanya akotangaka ‘elimo wa Nzambe’ lokola moto ale­ngeli maye Yezu akoyebisa mpo ya Eli­mo Santu, oyo azali Nzambe elo­ngo na Tata mpe na Mwana.
1,11.	Komono Nzambe mabe te mpo ya ndenge akoyangelaka molóngó mobimba.
1,13.	Awa mokomi akanisi liwa lya nzoto mpe liwa lya molimo : mpo ya lisumu lya ye moto akomi koyoka mawa mpe nsomo. Tokoki kotanga yango o Lib 2 na 3.
1,16.	Ba-Yuda baye baboyi eyamba mpe bakomi kolanda bisengo bya mokili, baye bakonyokoloko bato basusu mpe bakokinolaka Nzambe. Bapagano, baye ba-Yuda basusu bamekoli bizalela bya bango, bakotangemaka mpe ‘baye baboya Nzambe’.
2,9.	Baye bakotiaka mitema na Nza­mbe te bakomonoko ’te bomoi bwa bango bozangi ntina ; bakolukaka se kosepela makambo ma mokili.
2,23.	Tala Lib 1,26.
2,24.	Oyo batangi o Lib nyoka mose­nginyi, mokomi oyo atangi ye polele ‘zabolo’. Ye moto akotisi liwa lya seko o mokili. Baye bakondimela ye bakokufa liwa lya seko.
3,4.	Baye baboya Nzambe bakanisi ’te basemba bakokufa liwa lya mawa, nzokande bazali na elikya ’te bakozala o mboka Nzambe nsima ya liwa lya nzoto. Biso bakristu tokotangaka maloba maye (3,1-5) mpo ya kosakola ’te toyambi nsekwa ya bato (mbala mingi o misa ya bawa).
3,9.	Na maloba maye mokomi alingi kolakisa bolamu bwa baye bakomi o mboka Nzambe.
3,14.	Eniki akobotaka bana te, se lokola mwasi ekomba, kasi misala milamu mya bango mikokembisa mpe mikokumisa bango o miso ma Nzambe.
5,3.	Bobele o mokolo bakokata makambo ma bato banso, bato baboya Nzambe bakoyeba ’te balekisi ntango mpamba o mokili.
5,17.	Bazalaki kotala ‘mokolo mwa nsuka’ lokola etumba enene kati ya Nzambe na monguna nyonso wa ye ; o etumba eye minkalali, mbula ya matandala na ekumbaki bikosalisa Nza­mbe. Ye moko akolata bosembo bo molato mwa etumba,...(20-23). S. Polo akokoma bobele bongo mpo ya kolakisa bibuneli bya mokristu oyo ase­ngeli kolonga bobe.
7,2.	O ntango ena bazalaki kotanga sanza eye abandaki bobele sika lokola sanza mobimba.
7,7.	Tala 1 Bak 3,6-12.
7,20.	Tala 1 Bak 5,13.
9,1.	Losambo loye mpo ya kokoma na bwanya loulani mingi na losambo la Salomo (1 Bak 3,6-9 ; 2 Mkl 1,8-10).
9,18.	O eteni ya isato ya buku eye mokomi akolimbola ndenge bwanya ikobikisaka baye bakotiaka mitema na bwango.
10,1.	Mokomi atangi awa mikóló minene mya Israel ut’o eleko ya ba­nko­ko ; nzokande atangi nkombo ya bato te, mpo batangi baye bayebaki moko moko wa bango malamu : atangi bongo Adamu ‘tata wa bato’.
10,3.	Kain (Lib 4,1-16).
10,4.	Awa batangi Kain te, mpo nta­ngo ya mpela enene azalaki lisusu te ; batangi awa lokola moto moko bato babe banso bazalaki o mokili.
10,4.	Nowe (Lib 6 tee 9).
10,5.	Moto wa bosembo oyo, Abarama (Lib 15,6) oyo andimi kobonzela Nzambe mwana wa ye Izaka (Lib 22).
10,6.	Awa batangi likambo likweli Lot, oyo abiki ntango Sodoma na Gomora itumbami (Lib 19).
10,7.	Mwasi wa Lot (Lib 19,26).
10,10.	Ezalaki Yakob moto akimi Ezau (Lib 27,23). O 10,29-31 bayebisi na bokuse maye makweli Yakob epai ya noko wa ye Laban (Lib 29-31) mpe etumba ya ye na Nzambe o Yabok (Lib 32,23-33).
10,13.	Yozefu moto atekami na ba­ndeko ba ye (Lib 37-39).
10,15.	O 10,15-27 bapesi lisolo lya bobimi o Ezipeti mpe bitumbu bikweli ba-Ezipeti.
11,1.	Ezali Moze : Mbk 18,15-18.
11,4.	Tala Bob 17,1-7 ; Mit 20,2-13.
11,15.	Ba-Ezipeti bazalaki kolakisa banzambe ba bango na bikeko bya nyama. Na bolekisi ndelo, mokomi ayebisi ’te nyama iye bazalaki koku­mbamela ikomaki nyama ya zamba iye ibundisi bango.
12,5.	Ba-Kanana bazalaki kobonzela banzambe bana ba bango (Mbk 12,31 ; Nz 106,37-38), kasi toyebi te soko baliaki misuni mya bato.
12,11.	Nowe atombeli Kanana, mwa­na wa Kam mpe nkoko wa ba-Kanana, bobe (Lib 9,25).
13,1.	O eteni eye (1-9) balimboli ndenge bato bakoki koyeba Nzambe Mozalisi na bokamwi na bonzenga bwa bikelamo bya ye o mokili. Ekoki na bato bamituna soko biuti wapi, soko nani akeli byango. Bato basusu bakokumbamelaka bikelamo biye lokola banzambe ; bango bazali na mayele mingi te, solo. Kasi baye bakokumbamelaka bikeko biye bisalemi na maboko ma bato bazali na mayele ata moke te (13,10 - 14,31).
13,10.	Tala Iz 44,9-20 ; Yer 10,1-16 ; Ba 6.
14,6.	Nowe moto abikaki o masuwa, mpe Nzambe akomisi ye bongo nkoko wa molongo mwa sika mwa bato.
14,20.	Bakonzi ba Ezipeti mpe ba Siria bazalaki kotinda ’te bato bapesa bango lokumu lokoki na Nzambe, mpe bakumbamela bikeko biye bizalaki na bilongi bya bango moko. Tala Bik 12,22 esika bato baye babeteli mokonzi Erode Agripa maboko bagangi : « Azali koloba lokola nzambe ! »
14,22.	Likambo lya bokumbameli bikeko lizalaki mpe kobebisa boyokani o kati ya bato. Tala mpe Rom 1, 24-32, esika S. Polo afundi mabe ma bapagano.
15,2.	Ba-Israel batondi Nzambe mpo asali ’te balanda likambo lya bozoba lya bokumbameli bikeko te. Ata bazali na masumu, bakoki koboya Nzambe te, oyo akolingisaka bango ntango inso kozongela ye (11,23 - 12,2 ; 12,16-18), mpe akokisi bongo maye alakelaki bankoko ba bango (12,19-22).
15,7.	Lokola moto asali ekeko na eteni ya nzete (13,11-19), mosali mbeki akosalaka bikeko bya banzambe ba ye. Mbele bapagano mingi bayebaki ’te biloko biye bizalaki banzambe ba solo te, kasi bobele bilili bya bango ; kasi bazalaki kosala mabe, mpo na bikeko bya bango bazalaki kotumola Nzambe wa solo.
15,15.	Tala Nz 115,4-7.
16,1.	Banda awa tee nsuka ya buku eye (16-19) mokomi akobi lisolo liye asili abandaki (11,4 - 12,27) mpo ya kolakisa ndenge Nzambe atumbolaki ba-Ezipeti mpe asalaki ba-Israel malamu. Batangi baye bayebaki makambo maye malamu, mpe abakisi mpe maye mauti na masolo ma bato ba nsima, kasi alandaki na yango molongo mwa mikóló malamu te. Yango wana toyoki mwa kwokoso mpo ya kolimbola makambo manso.
16,2.	Tala Bob 16,9-13 ; Mit 11,10-32.
16,5.	Tala Mit 21,4-9.
16,17.	O Bob 9,23-24 batangi bobele mbula ya matandala, nkake na minkalali ; awa (16,17-20) babakisi makambo mwa mingi.
16,20.	Bilei biye, nde manu, eye mokomi alakisi lokola mampa elengi maye banzelu bakabelaki bato. Yezu akoba­nda na lisolo lya manu mpo ya koyebisa bato ’te ayeli bango mampa maye makopesa bango bomoi bwa seko (Yo 6,31...). Nsima ya ye bakristu bakoli­mbola maloba ma Bw 16,20 lokola mo­komi asili alakisi Ukaristya na bo­nkutu.
16,28.	Ba-Yuda bazalaki kosambela losambo la ntongo liboso ’te ntongo etana. Kasi toyebi te soko tokoki koloba ’te momeseno mwango mouti na likambo lya manu basengelaki kolokoto na nta ntongo (Bob 16,21), lokola ekomami awa.
18,3.	Tala Bob 13,21-22.
19,5.	Mai ma mbu maye makabwani mpo ya kolekisa ba-Israel, makozonga na nsima o esika mazalaki mpo ya kozindisa ba-Ezipeti.
19,14.	Liboso Yakob na bana ba ye bayambamaki malamu o Ezipeti ; kasi ntango ba-Israel bakomi mingi, ba-Ezipeti bakomi kobanga bango mpe bakomisi bango baombo (Bob 1). 