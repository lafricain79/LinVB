1,1.	–Tala Rom 1,1-7.
	–Nzambe Mobikisi wa biso : Bakotangaka Nzambe Tata ‘Mobikisi’ se lokola Kristu, zambi Kristu, mpo ya kokokisa mosala mwa libiki, asalaki se manso Tata alingaki.
1,2.	Timoteo : Tala 1 Kor 4,17.
1,4.	–Masapo : Tala Tito 1,14.
	–Milongo mya nkombo ya bankoko : Ba-Yuda basusu balingaki mingi koyeba myango kin’o eleko ya kala mpenza. Koyekola myango ekozwela moto litomba te o nzela ya boyambi.
1,5.	‘Motindo moye’ mozali to mobeko mwa bosangeli Nsango Elamu to mobeko moye Polo atangi o 1 Tim 1,3.
1,8.	Mobeko mwa Moze : Ba-Yuda bayebaki mobeko mosusu te. Mobeko mwa Moze mozalaki molamu, zambi mozalaki kolakisa bango ntina ya lisumu. Yango wana mobeko mozalaki kobangisa basumuki mpe kolendisa basemba.
1,20.	Santu Polo asalaki bongo na mokristu moko wa Korinti : Tala 1 Kor 5,5.
2,4.	Nzambe alingi ’te bato banso babika, ata baye bayebi ye te. Mpo ’te abika, moto nyonso asengeli kosala malamu, se lokola motema mwa ye mokolakisaka ye (Rom 2,14-16 ; Bik 10,35 ; Ebr 11,6).
2,5.	Mobembisi : montei (médiateur).
2,6.	Na mateya ma ye mpe na liwa lya ye o kuruse, Kristu ayei kolakisa ’te Nzambe alingi kobikisa bato banso (tala mpe 6,13).
2,11-12.	Mobeko moye motali se ma­kita ma Eklezya o mboka yoko yoko.
2,15.	–Polo ateyi ’te lokumu la mwasi lokokani na lokumu la mobali (tala Gal 3,28).
	–O 1 Kor 7,25-35 Polo ateyi boye : Epusi malamu mwasi ayeba mobali te. Kasi bato basusu batiaki mobulu o Ekle­zya, balobi ’te libala lizali mabe ; basusu bazalaki koloba ’te babali na basi basala se lokola bango moko balingi.
3,2.	Moyangeli asengeli kopesa ndakisa elamu o miso ma bato banso, liboso mpenza o maye matali libota lya ye. O eleko ena moyangeli oyo akufelaki mwasi, akokaki kobala lisusu te. Soko abalaki lisusu, baponoki moto mosusu o esika ya ye.
3,8.	Mosala mwa diakono mozalaki mingi kosalisa babola.
3,11.	Basusu babongoli : ‘badiakono basi’ (tala Rom 16,1).
3,12.	To : ‘Bazala na mwasi se moko’.
3,16.	Kristu : mbakisa ya biso mpo ya kolimbola.
4,1.	Mikolo mya nsuka : banda boyei bwa yambo bwa Kristu tee boyei bwa ye bwa mibale.
4,2.	O ntango ena bazalaki kotia ele-mbo ya móto o nzoto ya moombo oyo akimaki nkolo wa ye.
4,13.	O lisanga lyoko lyoko bakristu bazalaki kotanga eteni ya Minkandá Misantu. Nsima bazalaki kobakisa mateya ma bopaleli bandeko (tala Lk 4, 16-21 ; Bik 13,14-16 ; 17,2-3).
4,14.	Ntango batielaki yo maboko : Bakotiela moto maboko to mpo ya kobenisa (Mt 19,15) to mpo kobikisa (Mk 5,23 ; 6,5), to mpo ya kokulisa ye Elimo Santu ntango ya batisimo (Bik 19,6), to nsima ya batisimo (nkotisa : Bik 14-17). Ekoki mpe kolakisa ’te moto moko andi­mami o mosala mwa Eklezya (Bik 6,6) to atindami kosangela Nsango Elamu o mboka esusu (Bik 13,3). Bakolo bayingisi Timoteo o mosala mwa ye mwa Eklezya, bapesi ye likabo lya ngrasya lisengeli na ye mpo ’te akokisa mosala mwa ye bo ebongi.
5,3-16.	Basi bazenge (bakufeli babali) ba solo bazali na moto moko te, oyo akoki kosalisa bango, yango wana Eklezya ekosalisaka bango (5,3.5.16). Basusu, baye bazali naino na bandeko, basengeli kozwa lisalisi epai ya bandeko ba bango moko (5,4.8.16). Basusu naino bazali na mosala mokoki na bango o Eklezya : bazala na ezalela ebongi mpenza ; bongo Eklezya ekoka koko­ma bango o molongo mwa basi bazenge (5,9-15).
5,10.	Na bosukoli mompaya makolo, nkolo wa ndako alakisi ’te ayambi ye na motema boboto.
5,12.	Mokano mwa komibonza na Nzambe.
5,22.	Moto oyo aponomi mpo ’te asala mosala mwa Eklezya.
6,2.	Tala Ef 6,5 ; Kol 3,32 : Soko nkolo azali mopagano, moombo asalela ye na molende ; soko asali bongo te, mbele bapagano bakonyokolo Eklezya. Soko nkolo azali mokristu, atala moombo lokola ndeko wa ye, zambi o miso ma Eklezya bato banso bazali ba­nsomi.
6,12.	Toyebi te soko Polo alobi yango mpo ya batisimo ya Timoteo to mpo ya mokolo batielaki ye maboko.