\id JUD
\h Yuda
\toc1 Monkanda mwa santu Yuda
\toc2 Yuda
\toc3 Yuda
\mt1 Monkanda mwa santu Yuda
\imt Boyingisi
\is2 Lolenge mpe etindameli nani ?
\ip Monkanda mwa santu Yuda molakisami o molongo mwa yambo lokola « monkanda mozali na mateya ma lokuta », (Scheikle K.H. Die Petrusbriefe – Der Judashreif, Freiburg, 1970, lokasa 137). Makanisi ma apostolo mazali mpo ya kobatela lisanga lya bati­ndeli liye lizali na minyoko mya bokotisi makambo mabe. O kati ya makomi tomoni bosuani mpe bowelani bwa moto azali na likama mpe azali koluka kobombama. Yuda atindeli ba Eklezya biike monkanda moye ; moulani mingi na Monkanda mwa mibale mwa santu Petro. Ye mpe azali kokebisa bakristu ’te bandima mateya ma lokuta te, zambi mozali kobebisa bizalela bya bato. Na bateyi na bato ba koyamba mateya ma bango, bakozwa etumbu. Bakristu bapikama o nzela ya boyambi. Bango mpenza bazali kotiela bino libaku o bisika bokosa­nganaka mpo ya kolia limpati lya bondeko. Atindi monkanda na lisanga liye liyebani malamu mpenza te. Kasi ezali lisanga lya nzinga nzinga liye lizali na likama.
\is2 Mokomi mpe eleko
\ip Amilakisi lokola « Yuda », ndeko wa Yakobo (m. 1). Atako bapesi nkombo ibale eye o kati ya Bondeko bwa Sika, bolakisi bozali na ntina mpenza te. Ezali Yuda apostolo te (tala Lk 6,16 ; Bik 1,13). Elimboli mpenza te lisolo etangemi. Atako balakisi malamu te bokeseni o kati ya Yuda mpe na Yakobo « mpo ya Mokonzi Yezu Kristu » mpe bapostolo ndoyi, bakanisi te Yuda ndeko wa Yakobo nde mokomi wa monkanda. Moto atangemi o molongo mwa yambo o kati ya Eklezya ya Yeruzalem. Monkanda mokomami yambo ya monkanda mwa santu Petro wa mibale, kasi bino bandeko ba bolingo bokumisa naino maloba maye bapostolo ba Mokonzi wa biso Yezu Kristu balobaki (tala m. 17), Monkanda mwa santu Yuda nde mokomami o eleko pene ya nsuka ya sekulo ya yambo, pene ya mobu 90.
\is2 Bokokani na Petro wa babale
\ip Makomi ebele malakisi te minkanda o kati ya Petro wa babale, eteni ya mibale, molongo mwa yambo kino zomi na sambo mpe Yuda 4–13 bozali na boulani. Kasi boulani bona na lolenge nini ? Kasi balakisi nzela mpe bokokani bwa minkanda miye te.
\is2 Liteya
\ip O kati ya nzela bwa bomoi bwa mokristu, ngolu, boboto na bolingi bwa Nzambe botondi o mitema mya bino, wana nde elembo ya boyambi (m. 2), boyambi mpe ’te ata boyebi mambi maye manso malingi kokundolela bino ; Mokonzi abimisaki ekolo ya ye o mokili mwa Ezipiti, kasi na nsima abomi baye baboyaki koyamba ye (m. 3.9). Kasi bino, bandeko ba bolingo bopikama o eyamba esantu, bosambela na nguya ya Elimo Santu (m. 20).
\is Mwango
\ip Molongo 1–2 : losako.
\ip Molongo 3–16 : bokima bateyi ba lokuta.
\ip Molongo 17–23 : mateya ma bapostolo.
\ip Molongo 24–25 : maloba ma nsuka.
\c 1
\s1 Losáko
\p
\v 1 Ngáí Yúda, mosáleli wa Yézu mpé ndeko wa Yakóbo, nakomélí bínó bato Nzámbe abyángí, baye Nzámbe Tatá akolingaka mpé Yézu Krístu akobátelaka.
\v 2 Ngolu, bobóto ná bolingi bwa Nzámbe bótónda o mitéma mya bínó.
\s1 Bókíma batéyi ba lokutá
\p
\v 3 Bandeko ba bolingo, nalingákí míngi kokomela bínó makambo ma libíkí lya bísó bánso. Kasi sikáwa namóní ’te nasengélí kosála yangó mpô ya koléndisa bínó : bóbunda etumba ya boyambi, boye Nzámbe apésí sé mbala yŏ kó na basántu\f + \fr 1,3 \ft  Basantu : Tala Rom 1,7+.\f* ba yě.
\v 4 Bato basúsu basílí bamíkótísí na mayéle o ntéi ya bínó ; etúmbu bakátélí bangó esílí ekomámí út’o kala o Minkandá Misántu. Bangó babóngólí ngrásya ya Nzámbe wa bísó ékóma nzelá ya kosála mabé ; bakowánganaka Yézu Krístu, Motéyi mpé Mokonzi sé mŏ­kó wa bísó.
\v 5 Atâ boyébí mambí maye mánso, nalingí kokundolela bínó ’te Mokonzi abimísákí ekólo ya yě o mokili mwa Ezípeti, kasi na nsima abo­mákí baye babóyákí koyamba yě\f + \fr 1,5 \ft  Abomi bato baboyaki ye o eliki (tala 1 Kor 10,5).\f*.
\v 6 Sé bôngó, bǎnzelú baye babátélí lokúmu la bangó té mpé batíkí esíká bazalákí : Nzámbe akangí bangó na minyolólo sékô o esíká ya molílí makási téé mokolo mwa Likambo linéne.
\v 7 Ndéngé yŏ kó bato ba Sódoma, ba Gómora\f + \fr 1,7 \ft  Tala Lib 19,1-11.\f* mpé ba mbóka isúsu ya penepene bamípésí na bondúmbá mpé balukí koyébana na baye bazalí na nzóto ya bato té. Bŏ ndakisa o míso ma bato bánso, bazwí etúmbu ya kozíka na móto mwa sékô.
\v 8 Baye bakótélí bínó sikáwa bakosálaka sé bôngó : mpô ya ndóto iye bakolótoko bakobébisaka nzóto ya bangó, babóyí bokonzi bwa Nzámbe mpé bakotúkaka bilímo bya nkémbo.
\v 9 Atâ ǎnzelú mokúmi Míkael, áwa awélákí ntembe na zábolo mpô ya ebembe ya Móze, abá­ngákí kokitisa yě na nsóni o likambo, kasi alobákí bobélé : « Mokonzi ápésa yŏ etúmbu\f + \fr 1,9 \ft  Tala Za 3,2.\f*. »
\v 10 Kasi bangó bakotúkaka makambo maye bayébí té. Bayébí sé makambo ma nzóto lokóla nyama izángí mayéle ; makambo maye makosúkela bangó mabé.
\v 11 Mawa na bangó ! Balandí bobélé nzelá eye Kain alandáká ; mpô ya lokósó la mbóngo, lokóla Baláme\f + \fr 1,11 \ft  –Balame : Tala Mit 22,2.\f*, babúngámí na maloba ma bokósi ; batombókí lokóla Kóre\f + \ft –Kore : Tala Mit 16.\f*, bôngó bakokúfa lokóla yě.
\v 12 Bangó ndé bazalí kotíela bínó libakú o bisíká bokosanganaka mpô ya kolía li­mpáti lya bondeko : bakolíaka téé batóndí, nsóni té. Bazalí lokóla mampata mazángí mái matíndámí na mompepe, lokóla nzeté ibótí té o eleko ya mbuma, ikúfí kokúfa mpenzá mpô bapikólí yangó.
\v 13 Sé lokóla mbóngé ya mbú itombókí mpé ikobimisaka fúlu, bakobimisaka makambo ma nsóni. Bazalí lokóla minzóto mibúngí nzelá mikosúka o molílí makási sékô.
\v 14 Enok, nkóko wa nsambo o molongó mwa ba­nkóko bandá Adámu asakóláká mpô ya bangó : « Tálá Mokonzi ayéí na basántu ba yě nkóto na nkóto,
\v 15 mpô ’te ákáta makambo ma bato bánso. Akotúmbola baye bánso babóyí Nzámbe mpô ya mabé mánso basálí ntángo babóyákí kotósa yě, mpé mpô ya maloba mabé balobákí na yě. »
\v 16 Ntángo ínso bazalí kolobaloba mpé komílela, bakolandaka maye mitéma mya bangó milingí, monoko mwa bangó motóndí na maloba ma lofúndo\f + \fr 1,16 \ft  Tala 2 Pe 2,18 ; Dan 7,8.20 ; Lv 19,15.\f* mpé na malété mpô ya kozwa bilóko bya bato ba nkita.
\s1 Matéya ma bapóstolo
\p
\v 17 Kasi bínó, bandeko ba bolingo, bókanisa maloba maye bapóstolo ba Mokonzi wa bísó Yézu Krístu balobákí.
\v 18 Bayébísákí bínó : « O mikolo mya nsúka bato basúsu bakomónono, baye bakoseke bínó mpé bakolanda mpósá ya mitéma mya bangó. »
\v 19 Bangó bakokabola bato, bakozala sé na mayéle ma nzóto mpé bakozánga Elímo.
\s1 Malako mpé maloba ma nsúka
\p
\v 20 Kasi bínó, bandeko ba bolingo, bópikama o eyamba esántu, bósá­mbela na ngúyá ya Elímo Sántu,
\v 21 bóbátela bolingi bwa Nzámbe o mitéma mya bínó, áwa bozalí kozila ’te Mokonzi wa bísó Yézu Krístu akoyókela bínó ngolu mpé akopésa bínó bomoi bwa lobíko.
\v 22 Bósílisa ntembe ya baye bazalí kotatabana ;
\v 23 bóbíkisa basúsu bázíka na móto té ; bóyókela bato basúsu ngolu\f + \fr 1,23 \ft \f*, kasi bókéba mpé bóyina atâ bila­mbá bya bangó biye bikómí mbindo mpô ya masúmu ma bangó.
\v 24 Oyo akokí kobátela bínó mpô ’te bókwéya té, akokí kokamba bínó epái ya nkémbo ya yě lokóla bato bazángí mabé mpé batóndí na esengo.
\v 25 Yě Nzámbe sé mŏ kó azalí Mobíkisi wa bísó na lisálisi lya Mokonzi wa bísó Yézu Krístu. Na yě nkémbo, bokonzi, bokási ná ngúyá út’o libandela, sikáwa mpé o bileko bínso sékô ! Amen.
